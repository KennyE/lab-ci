#ifndef __AVG_H
#define __AVG_H

/** @return The average of two integers @ref a and @ref b. */
int integer_avg(int a, int b);

#endif //__AVG_H
